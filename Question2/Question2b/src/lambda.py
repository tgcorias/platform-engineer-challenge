import json

def handler(event, context):
    # Evento recibido como entrada (puede ser un JSON)
    print("Received event:", json.dumps(event))

    # Mensaje de respuesta de la función Lambda
    message = "Hello, World!!"

    # Devuelve una respuesta como un diccionario JSON
    return {
        'statusCode': 200,
        'body': json.dumps(message)
    }