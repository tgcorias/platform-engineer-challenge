
# Question 2 - Reliability

## 2.c) Rollback design
If you have to implement a rollback for the previous deployment service, explain how you can design it.
Consider the following requirements:
 -  You cannot use the above revision for the task definition.
 -  You can use any Gitlab workflow strategy or any ECS service feature.
 -  Ensure as minimal downtime as possible.

***Deliverable:*** .gitlab.ci snippet or write a detailed explanation.

  
  

### Resolution

I'll assume this exercise refers to how to handle rollbacks in Gitlab CI/CD. \
For this, I believe rollbacks can be handled in any Git repository by rolling back to the previous state of Git, triggering the previous state of the code. \
This can be automated by adding a stage to the gitlab-ci.yml file that detects a failure and triggers a script that automatically executes git commands to revert the changes back to a stable state. \
In case of an emergency, the same strategy can be done manually using git commands.