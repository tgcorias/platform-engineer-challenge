# Question 3 - Reliability
## 3.a) VPC Monitoring
Based on the previous exercise on VPCs design, develop monitoring ideas that ensure optimal performance and fast problem resolution.

● Identify three key metrics crucial for VPC monitoring. \
● Explain the setup of CloudWatch Alarms based on these metrics. \
● Specify additional AWS services or tools enhancing observability in the Networking Topology. \
● How to detect a failure of multi-region connectivity? \
● How to audit VPC Traffic?


### **About key metrics:**

I believe that there are several important things we can monitor in a VPC, but I think the three most important aspects to consider are: ***troubleshooting, security, and performance***.
To specifically monitoring a VPC, we can implement VPC Flow Logs, a service offered by most cloud providers that is used to collect traffic information within a VPC. This service provides valuable insights such as the source and destination, and packets of all traffic to and from the VPC. 

### **CloudWatch Alarm**
In this case, let's assume we want to set up a notification for each IP packet with a status of REJECTED. Additionally, let's assume the log group for VPC Flow Logs is already created. To simplify the explanation, the steps are specified using the AWS console, but they can be done with Terraform. 

Steps: 
 1. Within AWS CloudWatch, navigate to our Log group (the log group assigned for VPC Flow Logs). 
 2. In the Metric Filters tab, click on "Create Metric Filter". 
 3. Insert the following filter pattern: 
 `[version, account, eni, source, destination, srcport, destport, protocol, packets, bytes, starttime, endtime, action="REJECT", flowlogstatus]`
 1. The next step is to assign a ***Filter Name***, for example: denied-traffic.
 2. In the same step, Metric Details are requested, we can assign for example the following:\
 ***Metric Namespace:*** denied-traffic-metrics  \
 ***Metric Name:*** denied-traffic \
 ***Metric Value:*** 1 *(this is the value that is published to the metric when there is a match in the filter pattern)*
 1. Click Next and finally Create Metric Filter.
 2. Once created, go back to the Metric Filters tab and select the checkbox, then click on Create Alarm.
 3. This will open a new tab where we need to enter the following values:\
 ***Metric name:*** denied-traffic \
 ***Statistic:*** Sum \
 ***Period:*** 1 Minute \
 ***Threshold type:*** Static \
 ***Condition:*** ≥ (Greater/Equal) \
 ***than..:*** 1 \
 Then, click on ***Next***. 
 1.  Then: \
   ***Alarm state trigger:*** In Alarm  \
   ***SNS topic:*** Create new topic \
   ***Topic name:*** CloudWatch_Alarms_Denied_Traffic \
   ***Email endpoint:*** Specify your email address. Choose \
   ***Create topic***. \
   Then, click on ***Next***.
   
 2.  Finally, enter the desired name in ***Alarm name***. 

By following these steps in the AWS console, we will receive an email alert each time there is a REJECT event within our VPC.

### **Enhancing observability in the Networking Topology.**
There are other resources recommended by AWS to monitor traffic in the VPC. Such as:
- AWS Traffic Mirroring.
- Amazon VPC IP Address Manager (IPAM).
- Reachability Analyzer.
- Network Access Analyzer.

Personally, I think another very useful tool not mentioned in that AWS article is the AWS WAF service. This tool can help us keep track of every request directed to protected resources, information such as client IP, requested URI, HTTP methods, etc.
Additionally, specific event-based alerts can be configured for a quick response to potential threats and thus avoid overages and mitigate those threats. 
You could also implement grafana to improve observability.

Source: [AWS - Monitoring your VPC](https://docs.aws.amazon.com/vpc/latest/userguide/monitoring.html)

### **How to detect a failure of multi-region connectivity?**
There are many ways to detect this kind of issues, using alarms as mentioned in the above responses could be a good solution these alarms may give you hints, another solution could be Route 53 Health Checks, they are used as part of Multi-site active/active architectures as part of the disaster recovery strategy and can be very useful in this use case.

### **How to audit VPC Traffic?**
In this case I will assume that the question is about literally how to, and not about what you should seek in a VPC audit, because it depends on the situation you're in within your company.
You can audit your VPC Traffic using VPC Flow Logs to capture all the metadata about the IP traffic within the VPC, you can inspect and analyze all that data with Cloudwatch Logs Insights o Athena.


## 3.b) Lambda Monitoring
Based on the previous exercise on Lambdas, develop monitoring ideas that ensure optimal performance and fast problem resolution.\
● Identify three key metrics crucial for Lambda function monitoring.\
● Explain the setup of CloudWatch Alarms based on these metrics.\
● Specify additional AWS services or tools enhancing observability in the
serverless environment.\
● How to detect a lambda bottleneck?\
● How to accelerate Lambda's cold start?\

### **Key Metrics**
As I see it, the three key metrics for AWS Lambda are:
 - **Invocations & Errors:** Crucial to quickly identify and respond to potential issues in code or runtime environment.
 - **Duration**: It directly impacts the cost of running Lambda functions and helps detect performance issues.
 - **Throttles:** It helps you to ensure optimal performance and availability of your Lambda functions, very important to prevent timeout errors.

### **CloudWatch Alarms**
For this example, we will create an alarm for when a Lambda function fails.
In this case, let's assume we have a Lambda function that should never fail, and we also assume that we already have the SNS topic created. We will create this alarm from the AWS console to simplify the explanation.

Steps:
1. First, go to AWS CloudWatch -> Log Groups -> *your-lambda-log-group*.
2. In the ***Metric Filter*** tab, click on ***"Create Metric Filter"***.
3. For the ***Filter pattern***, select "ERROR".
4. Define a ***Filter name***.
5. Then, just like in step 3a above, define the ***Metric details***, the relevant part in this step is to set ***Metric Value: 1***.
6. Once created, go back to the Metric filters tab, select the checkbox of the filter we created, and then click on "***Create alarm***".
7. In this step, we will define the following: \
***Metric name:*** -the name of the metric we created above-\
***Statistic:*** "Sum"\
***Period:*** 5 minutes.
8. In the Conditions tab, we will do the same as we did for exercise 3a: \
***TheresholdType:*** Static \
***Whenever:*** Greater/Equal \
***Than:*** 1.
9. Finally, select In Alarm and choose the SNS topic, then assign a name to this alarm.


**If you want to improve your observability about your lambdas, maybe you could implement Grafana to improve the observability, also yo could take a look at AWS X-Ray mentioned below.**

### **Lambda Bottlenecks**
There are a couple of different ways to identify bottlenecks in Lambda functions. For example, could monitor the metrics invocation count, duration, and error rate using CloudWatch. A sudden increase in invocations or duration can be a symptom of bottlenecks. 
Another solution could also be to use AWS X-Ray to identify bottleneck issues and troubleshooting.

### **Cold Start Issue**

To deal with this issue, there are a few things that AWS recommends doing:
 - Increase the memory allocated to the Lambda function.
 - Minimize the size of your deployment package.
 - Optimize your Lambda function code to minimize the time that it takes to initialize.
 - Avoid complex computation at startup.
 - Configure provisioned concurrency. 
 - Minimize the complexity of your dependencies.
