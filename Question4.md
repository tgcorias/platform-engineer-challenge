# Question 4 - Security
## 4a) CI/CD Security
Enhance the previous exercise (2.a Container), covering security considerations to the GitLab CI/CD pipeline, adhering to the following requirements: 

● Include SAST detection. You could use an open source or built-in platform option. \
● Include an image vulnerability scanner. You could use, OSS or Cloud-native
option. \
Deliverable: .gitlab.ci snippet. Also include any relevant information and assumptions.

### Resolution
For this problem, I implemented a simple and effective solution. GitLab provides two templates that can be implemented in pipelines, one for each use case mentioned in the exercise.

 - ***SAST:*** For this case, I implemented the GitLab template that returns a JSON artifact with the findings of vulnerabilities. In GitLab's paid plans, this JSON can be viewed from the UI.
 - ***Image Scanning:*** Similarly to the previous point, GitLab offers a template that allows us to view the results of scanning vulnerabilities in the Docker image in the pipeline.

Snippet:
    
    (gitlab-ci.yml)
        include:
        -  template:  Jobs/SAST.gitlab-ci.yml
        -  template:  Jobs/Container-Scanning.gitlab-ci.yml
        -  template:  AWS/Deploy-ECS.gitlab-ci.yml

Source: \
[SAST](https://docs.gitlab.com/ee/user/application_security/sast/) \
[Image Scanning](https://docs.gitlab.com/ee/tutorials/container_scanning/)

## 4b) Basic AWS Security

If you were to create an AWS account from scratch, what would be the first 3 AWS services you would configure to ensure a basic security posture.

***Deliverable:*** explain with text or diagrams the answer, clarifying any assumptions made. Highlight the key benefits for all AWS services activated.

### Resolution

As a first step, let's assume that the account is completely empty and created from scratch. We'll also assume that we're going to have only one AWS account. \
These are the top three AWS services I would apply when creating an account from scratch:

 - ***AWS IAM:***  This service is used to create users and enforce the use of MFA (Multi-Factor Authentication) for each user. It's worth noting that when creating the AWS account, we should enable MFA for the root user and create a user with minimal permissions for the tasks we need to perform. This service also helps us follow the principle of least privilege for all created users.
 - ***AWS CloudTrail:*** This service records API calls within the AWS account and provides useful information about who did what and when, giving valuable data about this, such as requester IP.
 - ***AWS GuardDuty:*** Once we have the infrastructure designed and start implementing networking aspects, AWS GuardDuty can be used to continuously monitor and detect malicious activity by analyzing logs, DNS queries, and API activity. It also generates alerts about detected threats.