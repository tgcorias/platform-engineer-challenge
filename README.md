# Platform Engineer Challenge

In this repository, you can find all the solutions to the challenges presented in the email. \
Each exercise's resolution is named with the exercise number and its corresponding item, for example: Question1/Question-1a.svg. \
Inside the Question2, you will find subfolders, you will find a readme explaining in each one.
In Question3.MD and Question4.MD you can find all resolutions for each subitem of these questions.